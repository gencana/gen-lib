package com.gencana.android.genlib.utils

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast

/**
 * Created by Gen Cana on 02/07/2018
 */

fun toast(context : Context, message : String){
    createToast(context, message, Toast.LENGTH_SHORT)
}

fun toast(context : Context, @StringRes message : Int){
    createToast(context, message, Toast.LENGTH_SHORT)
}

fun toastLong(context : Context, message : String){
    createToast(context, message, Toast.LENGTH_SHORT)
}

fun toastLong(context : Context, @StringRes message : Int){
    createToast(context, message, Toast.LENGTH_SHORT)
}

private fun createToast(context : Context, message : String, duration : Int){
    Toast.makeText(context, message, duration).show()
}

private fun createToast(context : Context, @StringRes message : Int, duration : Int){
    Toast.makeText(context, message, duration).show()
}