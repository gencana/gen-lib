package com.gencana.android.genlib.views.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gencana.android.genlib.R;
import com.gencana.android.genlib.constant.QrAction;
import com.gencana.android.genlib.event.QrActionEvent;
import com.gencana.android.genlib.event.QrResultEvent;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Created by Gen Cana on 05/07/2018
 */
public class QrScanFragment extends Fragment implements BarcodeCallback{

    private CaptureManager capture;

    public static QrScanFragment newInstance() {
        return new QrScanFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_qr_scan, container, false);
        DecoratedBarcodeView barcodeScannerView = view.findViewById(R.id.view_decorated_barcode);
        barcodeScannerView.getBarcodeView().setMarginFraction(.25d);

        capture = new CaptureManager(getActivity(), barcodeScannerView);
        capture.initializeFromIntent(getActivity().getIntent(), savedInstanceState);
        capture.decode();

        barcodeScannerView.decodeContinuous(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
        capture.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        capture.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onDestroy() {
        if (capture != null)
            capture.onDestroy();

        super.onDestroy();
    }

    @Override
    public void barcodeResult(BarcodeResult result) {
        capture.onPause();
        if (result != null){
            EventBus.getDefault().post(new QrResultEvent(result.getText()));
        }

    }

    @Override
    public void possibleResultPoints(List<ResultPoint> resultPoints) {

    }

    @Subscribe
    public void onActionEvent(QrActionEvent event){
        switch (event.getQrAction()){
            case QrAction.START:
                capture.onResume();
                break;
            case QrAction.STOP:
                capture.onPause();
                break;
        }
    }
}
