package com.gencana.android.genlib.event;

import com.gencana.android.genlib.constant.QrAction;

/**
 * Created by Gen Cana on 05/07/2018
 */
public class QrActionEvent {

    private int qrAction;

    public QrActionEvent(@QrAction int qrAction) {
        this.qrAction = qrAction;
    }

    public int getQrAction() {
        return qrAction;
    }
}
