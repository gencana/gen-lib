package com.gencana.android.genlib.data.remote;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Gen Cana on 02/07/2018
 */
public class ApiHelper<S> {

    private static final long CONNECT_TIME_OUT = 30;

    private static ApiHelper sInstance;

    private Context mContext;

    private Builder builder;

    S apiService;

    private ApiHelper(){

    }

    private ApiHelper(Context context, Builder<S> builder) {
        this.mContext = context;
        this.builder = builder;
        this.setup(builder);
    }

    public static <S> void init(Context context, Builder<S> builder){
        if (sInstance == null){
            sInstance = new ApiHelper(context, builder);
        }

    }

    public static ApiHelper getInstance() {
        if (sInstance == null)
            throw new NullPointerException("ApiHelper must be initialized first. Call Builder.build()");

        return sInstance;
    }

    private void setup(Builder<S> builder) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(builder.baseUrl)
                .addConverterFactory(builder.converterFactory == null ?
                        GsonConverterFactory.create(gson) : builder.converterFactory )
                .addCallAdapterFactory(builder.adapterFactory == null ?
                        getCallAdapterFactory() : builder.adapterFactory)
                .client(createHttpClient(builder.interceptors))
                .build();
        this.apiService = retrofit.create(builder.apiServiceClass);
    }

    public S getApiService() {
        return apiService;
    }

    private OkHttpClient createHttpClient(Interceptor... interceptors) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(httpLoggingInterceptor)
                .connectTimeout(getReadTimeout(), TimeUnit.SECONDS)
                .readTimeout(getConnectTimeOut(), TimeUnit.SECONDS);

        if (interceptors != null){
            for(Interceptor interceptor : interceptors){
                httpClient.addInterceptor(interceptor);
            }
        }

        return httpClient.build();
    }

    private CallAdapter.Factory getCallAdapterFactory(){
        return RxJava2CallAdapterFactory.create();
    }

    private long getReadTimeout(){
        return CONNECT_TIME_OUT;
    }

    private long getConnectTimeOut(){
        return CONNECT_TIME_OUT;
    }

    public static class Builder<S> {

        private Context context;

        private String baseUrl;

        private Class<S> apiServiceClass;

        private long connectTimeOut;

        private long readTimeOut;

        private Interceptor[] interceptors;

        private Converter.Factory converterFactory;

        private CallAdapter.Factory adapterFactory;

        private Builder(){

        }

        public Builder(Context context, @NonNull String baseUrl, @NonNull Class<S> apiServiceClass){
            this.context = context;
            this.baseUrl = baseUrl;
            this.apiServiceClass = apiServiceClass;
        }

        public Builder setConnectTimeOut(long seconds){
            connectTimeOut = seconds;
            return this;
        }

        public Builder setReadTimeOut(long seconds){
            readTimeOut = seconds;
            return this;
        }

        public Builder addInterceptors(Interceptor... interceptors){
            this.interceptors = interceptors;
            return this;
        }

        public Builder addConverterFactory(Converter.Factory factory){
            this.converterFactory = converterFactory;
            return this;
        }

        public Builder addAdapterFactory(CallAdapter.Factory factory){
            this.adapterFactory = adapterFactory;
            return this;
        }

        public void build() {
            Builder<S> builder = new Builder<>(context, baseUrl, apiServiceClass);
            builder.baseUrl = this.baseUrl;
            builder.apiServiceClass = this.apiServiceClass;
            builder.connectTimeOut = this.connectTimeOut;
            builder.readTimeOut = this.readTimeOut;
            builder.interceptors = this.interceptors;
            builder.converterFactory = this.converterFactory;
            builder.adapterFactory = this.adapterFactory;
            init(context, builder);
        }
    }
}

