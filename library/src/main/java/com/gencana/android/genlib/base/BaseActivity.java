package com.gencana.android.genlib.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Gen Cana on 05/07/2018
 */
public abstract class BaseActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutView());
    }

    protected abstract @LayoutRes int getLayoutView();

    protected void replaceFragment(Fragment fragment, @IdRes int container) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fm.beginTransaction();
        ft.replace(container, fragment);
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commitAllowingStateLoss();
    }

}
