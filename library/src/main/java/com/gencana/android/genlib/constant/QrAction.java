package com.gencana.android.genlib.constant;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Gen Cana on 05/07/2018
 */

@Retention(RetentionPolicy.SOURCE)
@IntDef({QrAction.START, QrAction.STOP})
public @interface QrAction {

    int START = 1;

    int STOP = 2;

}

