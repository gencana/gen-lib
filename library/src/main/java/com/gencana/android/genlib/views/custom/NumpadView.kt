package com.gencana.android.genlib.views.custom

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.View
import android.widget.TextView

import com.gencana.android.genlib.R
import kotlinx.android.synthetic.main.view_numpad.view.*

/**
 * Created by Gen Cana on 04/07/2018
 */
class NumpadView : BaseView, View.OnClickListener {

    interface NumpadListener {

        fun onNumberClicked(txtView : TextView, value : String)

        fun onClearClicked(txtView : TextView)

        fun onEnterClicked(txtView : TextView)

    }

    private var  numpadListener : NumpadListener? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    override fun initViews(view: View) {
        txt_input.keyListener = null
        setClickListeners(txt_num_0, txt_num_1, txt_num_2,
                            txt_num_3, txt_num_4, txt_num_5,
                                txt_num_6, txt_num_7, txt_num_8,
                                    txt_num_9, txt_cancel, txt_enter)
    }

    fun setClickListeners (vararg views: View){
        for (view in views){
            view.setOnClickListener(this)
        }
    }

    override fun getLayoutView(): Int {
        return R.layout.view_numpad
    }

    override fun declareAttributes(typedArray: TypedArray) {

    }

    override fun getStyleableAttributes(): IntArray {
        return IntArray(0)
    }

    fun setNumpadListener(listener : NumpadListener){
        this.numpadListener = listener
    }

    override fun onClick(v: View) {
        v as TextView
        when (v.id){
            R.id.txt_cancel -> onClearClicked(v)
            R.id.txt_enter -> numpadListener?.onEnterClicked(v)
            else ->{
                txt_input.text.append(v.text)
                numpadListener?.onNumberClicked(v, v.text.toString())
            }
        }
    }

    private fun onClearClicked(v : TextView) {
        if (txt_input.length() > 0)
            txt_input.setText(txt_input.text.substring(0, txt_input.length()-1))

        numpadListener?.onClearClicked(v)
    }


}
