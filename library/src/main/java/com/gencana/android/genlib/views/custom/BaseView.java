package com.gencana.android.genlib.views.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;

/**
 * Created by Gen Cana on 04/07/2018
 */
public abstract class BaseView extends FrameLayout{

    public BaseView(@NonNull Context context) {
        this(context, null);
    }

    public BaseView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs){
        View view = LayoutInflater.from(getContext()).inflate(getLayoutView(), this, true);
        initViews(view);
        if (attrs != null){
            proccessAttributes(attrs);
        }
    }

    protected abstract void initViews(View view);

    protected abstract @LayoutRes int getLayoutView();

    private void proccessAttributes(@NonNull AttributeSet attrs) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
                attrs, getStyleableAttributes(), 0, 0);
        try{
            declareAttributes(typedArray);
        }finally {
            typedArray.recycle();
        }
    }

    protected abstract void declareAttributes(TypedArray typedArray);

    protected abstract int[] getStyleableAttributes();

}
