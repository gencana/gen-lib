package com.gencana.android.genlib.event;


/**
 * Created by Gen Cana on 05/07/2018
 */
public class QrResultEvent {

    private String qrCode;

    public QrResultEvent(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrCode() {
        return qrCode;
    }
}
